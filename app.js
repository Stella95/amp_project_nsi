/*
Copyright 2018 Google Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

var express = require('express');
var formidable = require('formidable');
var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());
app.set('view engine', 'html');
app.engine('html', require('hogan-express'));
app.locals.delimiters = '<% %>';

var axios = require('axios');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var AUTH_COOKIE_MAX_AGE = 1000 * 60 * 60 * 2; //2 hours

var products = [{
  "name": "Jeans"
}, {
  "name": "Hoody"
}, {
  "name": "Hat"
}];

var countries = {
  "items": [{
    "countries": [{
      name: "US"
    }, {
      name: "UK"
    }]
  }]
};

var skuToSizeAndPrice = {
  "1001": {
    "sizes": {
      "XS": 8.99,
      "S": 9.99,
    },
  },
  "1002": {
    "sizes": {
      "S": 10.99,
      "M": 12.99,
      "L": 14.99,
    },
  },
  "1010": {
    "sizes": {
      "L": 11.99,
      "XL": 13.99,
    },
  },
  "1014": {
    "sizes": {
      "M": 7.99,
      "L": 9.99,
      "XL": 11.99,
    },
  },
  "1015": {
    "sizes": {
      "XS": 8.99,
      "S": 10.99,
      "L": 15.99,
    },
  },
  "1016": {
    "sizes": {
      "S": 8.99,
      "L": 14.99,
      "XL": 11.99,
    },
  },
  "1021": {
    "sizes": {
      "XS": 8.99,
      "S": 9.99,
      "M": 12.99,
    },
  },
  "1030": {
    "sizes": {
      "M": 10.99,
      "L": 11.99,
    },
  },
};

var allLaptops = {
  "items": [
    {
      "id": 1,
      "model": "Acer Aspire A315-41G-R3S1",
      "cpu": "AMD Ryzen™ 7 2700U, 2.2-3.8 GHz, sa 2 MB keš memorije, 4 cores, 8 threads",
      "memorija": "8 GB DDR4 2400MHz, 2 memorijska slota",
      "HDD/SSD": "1 TB, 5400 rpm",
      "graficka": "AMD Radeon 535 sa 2GB GDDR5 sopstvene memorije",
      "ekran": "15.6 inch,FHD,LED, backlit,LCD, Non-Glare, 1920x1080 piksela",
      "mreza": "1x 10/100/1000 Ethernet LAN, WiFi 802.11ac, Bluetooth® 4.0",
      "baterija": "2-ćelije Li-Polymer",
      "cena": 79990,
      "slika": ""
    },
    {
      "id": 2,
      "model": "Acer A315-41-R5BB",
      "cpu": "AMD Ryzen™ 3 2200U, 2.5GHz - 3.4GHz, 4MB keš memorije, 2 jezgra, 4 thread-a",
      "memorija": "4GB SO-DIMM DDR4, 2400MHz, 2 slota, maksimalno 16GB",
      "HDD/SSD": "128GB SSD",
      "graficka": "Integrisana Radeon Vega 3",
      "ekran": "15.6 inch,FullHD, AG (AntiGlare), LED backlit HD,1920 x 1080 piksela",
      "mreza": "LAN, WiFi, 802.11b/g/n/ac, Bluetooth 4.2",
      "baterija": "Li-Ion 4 ćelije",
      "cena": 42190,
      "slika": ""
    },
    {
      "id": 3,
      "model": "Acer A315-41-R5GR",
      "cpu": "AMD Ryzen 3 2200U, 2.50 GHz - 3.40 GHz, 4 MB keš memorije, 2 jezgra, 4 thread-a",
      "memorija": "4 GB DDR4 SDRAM, 16 GB maksimum",
      "HDD/SSD": "1TB SATA HDD, 5400rpm",
      "graficka": "Integrisana AMD Radeon Vega 3",
      "ekran": "15.6 inch,Full HD LCD, ComfyView,1920 x 1080 piksela ",
      "mreza": "Gigabit Ethernet, WiFi 802.11ac, Bluetooth",
      "baterija": "Lithium Polymer, 4810 mAh",
      "cena": 48090,
      "slika": ""
    },
    {
      "id": 4,
      "model": "Asus UX391UA-EG020R ",
      "cpu": "Intel® Core™ i5-8250U Processor, 1.6-3.4 GHz, sa 6 MB keš memorije, 4 cores, 8 threads",
      "memorija": "8 GB LPDDR3",
      "HDD/SSD": "SATA3 256G M.2 SSD",
      "graficka": "Intel UHD Graphics 620",
      "ekran": "13.3 inch,Ultra Slim, FullHD, Anti-Glare,Wide View,1920x1080 piksela",
      "mreza": "WiFi 802.11ac+Bluetooth 4.2 (Dual band) 2*2",
      "baterija": "50WHrs, 2S2P, 4-ćelije Li-ion",
      "cena": 119990,
      "slika": ""
    },
    {
      "id": 5,
      "model": "Asus X540LA ",
      "cpu": "Intel® Core™ i3-5005U Processor, 2.0GHz, sa 3MB keš memorije, 2 cores. 4 threads",
      "memorija": "4GB LPDDR3",
      "HDD/SSD": "256GB SSD SATA3",
      "graficka": "Intel HD Graphics",
      "ekran": "15.6 inch,HD LED glare,1366x768 piksela",
      "mreza": "WiFi 802.11 b/g/n, Bluetooth 4.0",
      "baterija": "33WHrs, 3-ćelije Li-ion",
      "cena": 50990,
      "slika": ""
    },
    {
      "id": 6,
      "model": "Asus UX331UA-EG160T ",
      "cpu": "Intel® Core™ i5-7200U Processor, 2.5-3.1 GHz, sa 3 MB keš memorije, 2 cores, 4 threads",
      "memorija": "8GB LPDDR3, max 8GB",
      "HDD/SSD": "SATA3 256G M.2 SSD",
      "graficka": "Intel UHD Graphics 620",
      "ekran": "13.3 inch,Ultra Slim 300nits, FullHD, 16:9, Anti-Glare, NTSC:72%, Wide View,1920x1080 piksela",
      "mreza": "LAN 10/100/1000 Mbps, WiFi 802.11ac, Bluetooth 4.2 (Dual band) 2*2",
      "baterija": "50WHrs, 4S1P, 4-ćelije Li-ion",
      "cena": 84990,
      "slika": ""
    },
    {
      "id": 7,
      "model": "HP 15-da0056nm",
      "cpu": "Intel® Celeron® N4000 Processor, 1.1-2.6 GHz, sa 4MB keš memorije, 2 cores, 2 threads",
      "memorija": "4 GB DDR4-2400 SDRAM (1 x 4 GB)",
      "HDD/SSD": "500 GB 5400 rpm SATA",
      "graficka": "Intel® UHD Graphics 600",
      "ekran": "SVA, anti-glare, WLED-backlit",
      "mreza": "10/100/1000 GbE LAN, Realtek RTL8821CE 802.11b/g/n/ac (1x1) Wi-Fi®, Bluetooth® 4.2 Combo",
      "baterija": "3-ćelije, 41 Wh Li-ion",
      "cena": 34990,
      "slika": ""
    },
    {
      "id": 8,
      "model": "HP 15-db0016nm,",
      "cpu": "AMD Ryzen™ 5 2500U, 2-3.6 GHz, sa 2 MB L2 i 4 MB L3 keš memorije, 4 cores, 8 threads",
      "memorija": "4 GB DDR4-2400 SDRAM (1 x 4 GB)",
      "HDD/SSD": "1 TB 5400 rpm SATA",
      "graficka": "AMD Radeon™ Vega 8 Graphics",
      "ekran": "15.6 inch,HD, SVA, Anti-glare, WLED-backlit,1366x768 piksela",
      "mreza": "Integrated 10/100/1000 GbE LAN, 802.11b/g/n/ac (1x1) Wi-Fi, Bluetooth® 4.2 Combo",
      "baterija": "3-ćelije, 41 Wh Li-ion",
      "cena": 54990,
      "slika": ""
    },
    {
      "id": 9,
      "model": "HP 250 G6 (1WY37EA)",
      "cpu": "Intel® Core™ i7-7500U, 2.70 GHz - 3.50 GHz, 4 MB SmartCache, 2 cores, 4 threads",
      "memorija": "8 GB DDR4-2133 SDRAM (1 x 8 GB)",
      "HDD/SSD": "256GB SSD",
      "graficka": "Intel HD Graphics",
      "ekran": "15.6 inch,Intel HD Graphics,1920x1080 piksela",
      "mreza": "WiFi b/g/n, GigaEthernet 10/100/1000",
      "baterija": "4 ćelije Lithium-Ion",
      "cena": 93590,
      "slika": ""
    }
  ]
};

app.get('/shirts/sizesAndPrices', function (req, res) {
  var sku = req.query.sku;
  var response = {};
  response[sku] = skuToSizeAndPrice[sku];
  setTimeout(() => res.json(response), 1000); // Simulate network delay.
});

app.get('/countries.json', function (req, res) {
  assertCors(req, res);
  res.send(countries);
});

app.get('/shipping', function (req, res) {
  assertCors(req, res);
  var nextDayAvailability = {};
  var location = req.query.location;
  if (location && location == 'UK') {
    nextDayAvailability = {
      "next-day-availability": "not available"
    }
  } else {
    nextDayAvailability = {
      "next-day-availability": "available"
    }
  }
  res.send(nextDayAvailability)
});

app.get('/autosuggest/search_list', function (req, res) {

  var query = req.query.q;
  var response = {};
  var new_obj_array = products.filter(function (obj) {
    if (obj.name.toLowerCase().includes(query.toLowerCase())) {
      return true;
    }

    return false;
  });
  response = new_obj_array;
  assertCors(req, res);
  res.json(response);
});

app.get('/login', function (req, res) {
  assertCors(req, res);
  var returnUrl = req.query.return;
  res.render('login.html', {
    returnUrl: req.query.return
  });
});

app.post('/submit', function (req, res) {
  var email = req.body.email;
  var password = req.body.password;
  var returnUrl = req.body.returnurl;
  assertCors(req, res);

  // set user as logged in via cookie
  res.cookie('ampEmailLogin', email, {
    maxAge: AUTH_COOKIE_MAX_AGE // 2hr
  });
  res.redirect(returnUrl + '#success=true');
});

app.get('/logout', function (req, res) {
  var email = req.cookies.ampEmailLogin;
  if (email) {
    res.clearCookie('ampEmailLogin');
  }
  assertCors(req, res);
  res.redirect(req.header('Referer') || '/');
});

app.get('/authorization', function (req, res) {
  var response = {};
  var deals = {};
  if (req.cookies.ampEmailLogin) {
    if (req.cookies.ampEmailLogin == 'test1@example.com') {
      deals = [{
        link: "/deals/tshirt-test1.jpg",
        img: "/deals/tshirt-test1.jpg",
        name: "tshirt"
      }, {
        link: "/deals/hoody-test1.jpg",
        img: "/deals/hoody-test1.jpg",
        name: "hoody"
      }]
    } else {
      deals = [{
        link: "/deals/hoody-test2.jpg",
        img: "/deals/hoody-test2.jpg",
        name: "hoody"
      }]
    }
    response = {
      'return': true,
      'loggedIn': true,
      'deals': deals,
      'name': req.cookies.ampEmailLogin.replace("@example.com", "")
    };
  } else {
    response = {
      'return': true,
      'loggedIn': false
    };
  }
  assertCors(req, res);
  res.json(response);

});

app.get("/location-specific-results.json", function (req, res) {
  var ip = 'google.com';

  axios.get('https://freegeoip.net/json/' + ip).then(response => {
    assertCors(req, res);
    res.send({
      location: response.data.city
    });
  });
});

app.post('/shirts/addToCart', function (req, res) {
  // Necessary for AMP CORS security protocol.
  // @see https://github.com/ampproject/amphtml/blob/master/spec/amp-cors-requests.md
  assertCors(req, res);

  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields) {
    if (fields.color && fields.size && fields.quantity) {
      res.status(200).json(fields);
    } else {
      res.status(400).json({
        error: 'Please select a size.'
      });
    }
  });
});


//----------------------------------------------------
//MY METHODS ARE HERE
//----------------------------------------------------

app.post('/laptop/addToCart', function (req, res) {
  // Necessary for AMP CORS security protocol.
  // @see https://github.com/ampproject/amphtml/blob/master/spec/amp-cors-requests.md
  assertCors(req, res);

  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields) {
    if (fields.laptopid) {
      console.log(fields);

      if (!req.cookies.chart) {
        console.log("Cookie doesnt exists");
        var products = { items: [] };
        products.items.push(fields.laptopid);
        res.cookie('chart', products, {
          maxAge: AUTH_COOKIE_MAX_AGE // 2hr
        });
      }
      else if (req.cookies.chart.items.indexOf(fields.laptopid) == -1) {
        req.cookies.chart.items.push(fields.laptopid);
        res.clearCookie("chart");
        res.cookie("chart", req.cookies.chart, {
          maxAge: AUTH_COOKIE_MAX_AGE // 2hr
        });
        console.log("request cookies  " + req.cookies.chart.items);
      }




      res.status(200).json(fields);
    } else {
      res.status(400).json({
        error: 'Please select a size.'
      });
    }
  });
});

app.post('/laptop/removeFromCart', function (req, res) {
  // Necessary for AMP CORS security protocol.
  // @see https://github.com/ampproject/amphtml/blob/master/spec/amp-cors-requests.md
  assertCors(req, res);

  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields) {
    if (fields.laptopid) {

      console.log("###REMOVE FROM CHART#####");
      console.log("cookie before removal " + req.cookies.chart.items);

      var idToRemove = req.cookies.chart.items.indexOf(fields.laptopid);
      console.log("id of element to remove : " + idToRemove);
      var newArray = req.cookies.chart.items.splice(idToRemove, 1);
      console.log("new Array is : " + newArray);
      //req.cookies.chart.items=newArray;
      console.log("cookie after removal " + req.cookies.chart.items);


      console.log("###################")


      res.clearCookie("chart");
      res.cookie("chart", req.cookies.chart, {
        maxAge: AUTH_COOKIE_MAX_AGE // 2hr
      });

      res.status(200).json(fields);
    } else {
      res.status(400).json({
        error: 'Sorry, something bad happened to the server.'
      });
    }
  });
});

app.get('/laptop/getChart', function (req, res) {
  assertCors(req, res);
  var response = { items: [] };
  var chart = { items: [], sum: 0 };


  if (req.cookies.chart) {
    var ids = req.cookies.chart.items;
    var pom = 0;//holds full price of all purchased items
    ids.forEach(function (value) {
      chart.items.push(allLaptops.items[value - 1]);
      pom = pom + allLaptops.items[value - 1].cena;
    });
    if (pom == 0)
      chart.noItems = "Korpa je prazna";
    else
      chart.sum = pom.toLocaleString("en").replace(",", ".");
  }
  else {
    chart.noItems = "Korpa je prazna";
  }
  console.log("CHART : "+ chart.noItems);
  response.items.push(chart);
  res.status(200).json(response);

});

app.get('/laptop/getTotal', function (req, res) {
  assertCors(req, res);
  var response={ total: 0, sum: 0,shipping:0 };
  if (req.cookies.chart) {
    var ids = req.cookies.chart.items;
    var pom = 0;//holds full price of all purchased items
    ids.forEach(function (value) {
      pom = pom + allLaptops.items[value - 1].cena;
    });
    response.sum=pom;
    response.total=pom;
    console.log("Get total returned :"+response.total);

  }
  res.status(200).json(response);
});


app.post('/favorite', function (req, res) {
  var id = req.query.id;
  assertCors(req, res);
  if (req.cookies.ampFavorite && req.cookies.ampFavorite == 'true') {
    res.clearCookie('ampFavorite');
    res.cookie('ampFavorite', false);
    res.json(false)
  } else {
    res.cookie('ampFavorite', true);
    res.json(true)
  }
});

app.get('/favorite', function (req, res) {
  var id = req.query.id;
  assertCors(req, res);
  if (req.cookies.name == 'ampFavorite') {
    res.json(true)
  } else {
    res.json(false)
  }
});

function assertCors(req, res, opt_validMethods, opt_exposeHeaders) {
  const validMethods = opt_validMethods || ['GET', 'POST', 'OPTIONS'];
  const invalidMethod = req.method + ' method is not allowed. Use POST.';
  const invalidOrigin = 'Origin header is invalid.';
  const invalidSourceOrigin = '__amp_source_origin parameter is invalid.';
  const unauthorized = 'Unauthorized Request';
  var origin;

  if (validMethods.indexOf(req.method) == -1) {
    res.statusCode = 405;
    res.end(JSON.stringify({
      message: invalidMethod
    }));
    throw invalidMethod;
  }

  if (req.headers.origin) {
    origin = req.headers.origin;

  } else if (req.headers['amp-same-origin'] == 'true') {
    origin = getUrlPrefix(req);
  } else {
    return;
  }

  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Origin', origin);
  res.setHeader('Access-Control-Expose-Headers', [
    'AMP-Access-Control-Allow-Source-Origin'
  ]
    .concat(opt_exposeHeaders || []).join(', '));
  if (req.query.__amp_source_origin) {
    res.setHeader('AMP-Access-Control-Allow-Source-Origin',
      req.query.__amp_source_origin);
  } else {
    res.setHeader('AMP-Access-Control-Allow-Source-Origin',
      origin);
  }
}

function getUrlPrefix(req) {
  return req.protocol + '://' + req.headers.host;
}

//this here is root folder from which all files are served. 
app.use('/', express.static('e-commerce'));

app.listen(3000, function () {
  console.log(
    'Server for "E-commerce AMP" codelab listening on port 3000!'
  );
});
